<?php

namespace Lukasz_Juraszek\Miniblog\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

// deklaracja korzystania z modelu użytkownika
use App\User;

class PostController extends Controller
{
    /**
     * Wyświetlanie listy wpisów
     */
    public function index()
    {

        return 'index()';

    }

    /**
     * Pokazanie formularza do utworzenia nowego wpisu
     */
    public function create()
    {

        return 'create()';

    }

    /**
     * Przechwytywanie danych POST do utworzenia nowego wpisu
     */
    public function store(Request $request)
    {

        return 'store()';

    }

    /**
     * Wyświetlanie szczegółów wpisu
     */
    public function show($id)
    {

        return 'show()';

    }

    /**
     * Pokazanie formularza do edycji wpisu
     */
    public function edit($id)
    {

        return 'edit()';

    }

    /**
     * Aktualizacja wpisu z przechwytanych danych metodą PUT lub PATCH
     */
    public function update(Request $request, $id)
    {

        return 'update()';

    }

    /**
     * Usunięcie wybranego wpisu z bazy danych
     */
    public function destroy($id)
    {

        return 'destroy()';

    }
}