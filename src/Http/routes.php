<?php

Route::group(['middleware' => ['web']], function () {

    Route::resource(config('miniblog.prefix-route'), 'Lukasz_Juraszek\Miniblog\Http\Controllers\PostController');

});