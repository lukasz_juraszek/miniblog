<?php

// przestrzeń nazw zgodna z nazwą użytkownika/nazwa paczki
namespace Lukasz_Juraszek\Miniblog;

// deklaracja dziedziczenia klasy ServiceProvider
use Illuminate\Support\ServiceProvider;

class MiniblogServiceProvider extends ServiceProvider
{
    /**
     * Publiczna funkcja ładowania plików dodatku (widoki, migracje, pliki konfiguracyjne)
     * @return void
     */
    public function boot()
    {
        // kopiowanie pliku konfiguracyjnego
        $this->publishes([__DIR__ . '/../config/miniblog.php' => config_path('miniblog.php')]);
    }

    /**
     * Funkcja rejestracji dodatku w laravel
     * @return void
     */
    public function register()
    {
        include __DIR__ . '/Http/routes.php';

        $this->app->make('Lukasz_Juraszek\Miniblog\Http\Controllers\PostController');
    }
}